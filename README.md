* https://docs.ansible.com/ansible/latest/index.html
* https://galaxy.ansible.com/
* https://jinja.palletsprojects.com/en/2.11.x/
* https://www.rundeck.com/open-source
* https://github.com/ansible/awx/blob/devel/INSTALL.md#installing-awx
* https://galaxy.ansible.com/fortinet/fortios
